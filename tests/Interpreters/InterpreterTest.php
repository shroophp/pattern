<?php

namespace ShrooPHP\Framework\Tests\Request\Pattern\Interpreters;

use ShrooPHP\Pattern\Interpreter;
use ShrooPHP\Pattern\InterpreterException;
use PHPUnit\Framework\TestCase;

/**
 * A test case for
 * \ShrooPHP\Framework\Tests\Request\Pattern\Interpreters\Interpreter.
 */
class InterpreterTest extends TestCase
{

	/**
	 * Asserts that the default behaviour of the interpreter is expected.
	 */
	public function testDefaultBehaviour()
	{
		$pattern = '\\{}\}\\\\p@tt3r|\|{{\|<3y\}\\\\}|\/|@t(h{\{';

		$interpreter = new Interpreter;
		$interpretation = $interpreter->interpret($pattern);
		$args = $interpretation->match('{}\}\\\\p@tt3r|\|}\/@|u3\\{|\/|@t(h!');
		$expected = array('{\\|<3y}\\\\' => '}\/@|u3\\{', '\{' => '!');

		$this->assertNull($interpretation->match(''));
		$this->assertEquals($expected, $args);
	}

	/**
	 * Asserts that customized tokens cause expected behavior.
	 */
	public function testCustomizedTokens()
	{
		$pattern = '#()#)##p@tt3r|\|((#|<3y#)##)|\/|@t{h(#(';

		$interpreter = new Interpreter('(', ')', '#');
		$interpretation = $interpreter->interpret($pattern);
		$args = $interpretation->match('()#)##p@tt3r|\|)\/@|u3#(|\/|@t{h!');
		$expected = array('(#|<3y)##' => ')\/@|u3#(', '#(' => '!');

		$this->assertNull($interpretation->match(''));
		$this->assertEquals($expected, $args);
	}

	/**
	 * Asserts that the first of sequential groups is evaluated lazily.
	 */
	public function testSequentialGroups()
	{
		$subject = 'sequential';
		$interpreter = new Interpreter;
		$args = $interpreter->interpret('{1}{2}')->match($subject);
		$this->assertEquals(array('1' => '', '2' => $subject), $args);
	}

	/**
	 * Asserts that specifying an escape token indistinct from the begin key
	 * declaration token causes an appropriate exception to be thrown.
	 */
	public function testIndistinctBeginToken()
	{
		$code = InterpreterException::BEGIN_INDISTINCT;
		$this->assertInterpreterException('!', '?', '!', $code);
	}

	/**
	 * Asserts that specifying an escape token indistinct from the end key
	 * declaration token causes an appropriate exception to be thrown.
	 */
	public function testIndistinctEndToken()
	{
		$code = InterpreterException::END_INDISTINCT;
		$this->assertInterpreterException('?', '!', '!', $code);
	}

	/**
	 * Asserts that specifying a begin token that is too short causes an
	 * appropriate exception to be thrown.
	 */
	public function testBeginTokenTooShort()
	{
		$code = InterpreterException::BEGIN_LENGTH;
		$this->assertInterpreterException('', '?', '?', $code);
	}

	/**
	 * Asserts that specifying a begin token that is too long causes an
	 * appropriate exception to be thrown.
	 */
	public function testBeginTokenTooLong()
	{
		$code = InterpreterException::BEGIN_LENGTH;
		$this->assertInterpreterException('!!', '?', '?', $code);
	}

	/**
	 * Asserts that specifying an end token that is too short causes an
	 * appropriate exception to be thrown.
	 */
	public function testEndTokenTooShort()
	{
		$code = InterpreterException::END_LENGTH;
		$this->assertInterpreterException('?', '', '?', $code);
	}

	/**
	 * Asserts that specifying an end token that is too long causes an
	 * appropriate exception to be thrown.
	 */
	public function testEndTokenTooLong()
	{
		$code = InterpreterException::END_LENGTH;
		$this->assertInterpreterException('?', '!!', '?', $code);
	}

	/**
	 * Asserts that specifying an escape token that is too short causes an
	 * appropriate exception to be thrown.
	 */
	public function testEscapeTokenTooShort()
	{
		$code = InterpreterException::ESCAPE_LENGTH;
		$this->assertInterpreterException('?', '?', '', $code);
	}

	/**
	 * Asserts that specifying an escape token that is too long causes an
	 * appropriate exception to be thrown.
	 */
	public function testEscapeTokenTooLong()
	{
		$code = InterpreterException::ESCAPE_LENGTH;
		$this->assertInterpreterException('?', '?', '!!', $code);
	}

	/**
	 * Asserts that an interpreter exception with the given code is thrown when
	 * an interpreter is constructed with the given tokens.
	 *
	 * @param string $begin the token to begin key declarations with
	 * @param string $end the token to end key declarations with
	 * @param string $escape the token to escape key declaration tokens with
	 * @param int $code the expected code of the exception
	 */
	private function assertInterpreterException($begin, $end, $escape, $code)
	{
		$actual = null;

		try {
			new Interpreter($begin, $end, $escape);
		} catch (InterpreterException $e) {
			$actual = $e->getCode();
		}

		$this->assertEquals($code, $actual);
	}
}
