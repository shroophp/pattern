<?php

namespace ShrooPHP\Framework\Tests\Request\Pattern\Interpretations;

use ShrooPHP\Pattern\Interpretation;
use ShrooPHP\Pattern\InterpretationException;
use PHPUnit\Framework\TestCase;

/**
 * A test case for
 * \ShrooPHP\Framework\Tests\Request\Pattern\Interpretations\Interpretation.
 */
class InterpretationTest extends TestCase
{
	/**
	 * Asserts that the constructor of the interpretation has expected
	 * behavior.
	 */
	public function testConstructor()
	{
		$interpretation = new Interpretation('//', array());
		$this->assertEquals(array(), $interpretation->match(''));

		// Coverage for caching mechanisms.
		$this->assertEquals(array(), $interpretation->match(''));
	}

	/**
	 * Asserts that an invalid regular expression causes an appropriate
	 * exception to be thrown.
	 */
	public function testInvalidRegex()
	{
		$exception = null;
		$code = InterpretationException::REGEX;
		$interpretation = new Interpretation('', array());

		try {
			/** @scrutinizer ignore-unhandled */
			@$interpretation->match('subject');
		} catch (InterpretationException $exception) {
			$this->assertEquals($code, $exception->getCode());
		}

		$this->assertNotNull($exception);
	}

	/**
	 * Asserts that invalid keys cause an appropriate exception to be thrown.
	 */
	public function testInvalidKeys()
	{
		$code = InterpretationException::KEYS;

		$args = array(
			array('//',         'first'),
			array('/(.*)(.*)/', 'first'),
			array('/(.*)/',     'first', 'second'),
		);

		foreach ($args as $arg) {

			$exception = null;
			$interpretation = new Interpretation($arg[0], array_slice($arg, 1));

			try {
				$interpretation->match('subject');
			} catch (InterpretationException $exception) {
				$this->assertEquals($code, $exception->getCode());
			}

			$this->assertNotNull($exception);
		}
	}

	/**
	 * Asserts that the pattern matching of the interpretation has expected
	 * behavior.
	 */
	public function testPatternMatching()
	{
		$array = array('first' => 'pattern', 'second' => 'match');
		$subject = 'pattern,match';
		$regex = '/(.*),(.*)/';
		$keys = array('first', 'second');
		$interpretation = new Interpretation($regex, $keys);

		$this->assertNull($interpretation->match(''));
		$this->assertEquals($array, $interpretation->match($subject));
	}

}
