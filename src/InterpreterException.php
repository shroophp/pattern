<?php

namespace ShrooPHP\Pattern;

use Exception;
use InvalidArgumentException;

/**
 * An exception relating to the configuration of an interpreter.
 */
class InterpreterException extends InvalidArgumentException
{
	/**
	 * The value of the code indication that the begin key declaration token is
	 * not a single character.
	 */
	const BEGIN_LENGTH = 1;

	/**
	 * The value of the code indicating that the begin key declaration token is
	 * indistinct from the escape token.
	 */
	const BEGIN_INDISTINCT = 2;

	/**
	 * The value of the code indication that the end key declaration token is
	 * not a single character.
	 */
	const END_LENGTH = 3;

	/**
	 * The value of the code indicating that the end key declaration token is
	 * indistinct from the escape token.
	 */
	const END_INDISTINCT = 4;

	/**
	 * The value of the code indication that the end key declaration token is
	 * not a single character.
	 */
	const ESCAPE_LENGTH = 5;

	/**
	 * Constructs an exception relating to the configuration of an interpreter.
	 *
	 * @param int $code the exception code (as one of the class constant
	 * values)
	 * @param Exception $previous the previous exception used for the exception
	 * chaining
	 */
	public function __construct($code, Exception $previous = null)
	{
		parent::__construct(self::toMessage($code), $code, $previous);
	}

	/**
	 * Converts the given class constant value to its respective message
	 *
	 * @param int $code the class constant value to convert
	 * @return string the converted class constant value
	 */
	private static function toMessage($code)
	{
		$message = '';

		switch ($code)
		{
			case self::BEGIN_LENGTH:
				$message = 'The begin token must be a string containing one character';
				break;
			case self::BEGIN_INDISTINCT:
				$message .= 'The begin token must be distinct from the escape token';
				break;
			case self::END_LENGTH:
				$message = 'The end token must be a string containing one character';
				break;
			case self::END_INDISTINCT:
				$message .= 'The end token must be distinct from the escape token';
				break;
			case self::ESCAPE_LENGTH:
				$message = 'The escape token must be a string containing one character';
				break;
		}

		return $message;
	}
}
