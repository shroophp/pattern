<?php

namespace ShrooPHP\Pattern;

use Exception;
use InvalidArgumentException;

/**
 * An exception relating to the matching of a subject within an interpretation.
 */
class InterpretationException extends InvalidArgumentException
{
	/**
	 * the value of the code indicating an invalid regular expression
	 */
	const REGEX = 1;

	/**
	 * the value of the code indicating an invalid number of keys
	 */
	const KEYS  = 2;

	/**
	 * Constructs an exception relating to the matching of a subject within an
	 * interpretation.
	 *
	 * @param int $code the exception code (as one of the class constant
	 * values)
	 * @param Exception $previous the previous exception used for the exception
	 * chaining
	 */
	public function __construct($code, Exception $previous = null)
	{
		parent::__construct(self::toMessage($code), $code, $previous);
	}

	/**
	 * Converts the given class constant value to its respective message.
	 *
	 * @param int $code the class constant value to convert
	 * @return string the converted class constant value
	 */
	private static function toMessage($code)
	{
		$message = '';

		switch ($code) {
			case self::REGEX:
				$message .= 'Invalid regular expression';
				break;
			case self::KEYS:
				$message .= 'The number of specifed keys does not equal the';
				$message .= ' number of matched subpatterns';
				break;
		}

		return $message;
	}
}
