<?php

namespace ShrooPHP\Pattern;

use ShrooPHP\Pattern\InterpretationException;
use ShrooPHP\Core\Pattern\Interpretation as IInterpretation;

/**
 * An interpretation of a pattern.
 */
class Interpretation implements IInterpretation
{
	/**
	 * @var string the regular expression representing the pattern
	 */
	private $regex;

	/**
	 * @var array the keys within the pattern
	 */
	private $keys;

	/**
	 * @var int the total number of keys in the pattern
	 */
	private $total;

	/**
	 * Constructs an interpretation of a pattern.
	 *
	 * @param string $regex the regular expression to represent the pattern as
	 * @param array $keys the keys within the pattern
	 */
	public function __construct($regex, array $keys)
	{
		$this->regex = $regex;
		$this->keys = $keys;
	}

	public function match($subject)
	{
		$array = null;
		$values = array();
		$keys = $this->keys;
		$found = preg_match($this->regex, $subject, $values);

		if (false === $found) {
			throw new InterpretationException(InterpretationException::REGEX);
		}

		if ($found) {

			$total = count($values) - 1;

			if (is_null($this->total)) {
				$this->total = count($keys);
			}

			if ($total != $this->total) {
				$code = InterpretationException::KEYS;
				throw new InterpretationException($code);
			}

			$array = array();

			if ($total && !empty($keys)) {
				$array = array_combine($keys, array_slice($values, 1, $total));
			}

		}

		return $array;
	}
}
