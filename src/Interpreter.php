<?php

namespace ShrooPHP\Pattern;

use ShrooPHP\Core\Pattern\Interpreter as IInterpreter;
use ShrooPHP\Pattern\Interpretation;
use ShrooPHP\Pattern\InterpreterException;

/**
 * An interpreter of patterns.
 */
class Interpreter implements IInterpreter
{

	/**
	 * the default token that begins a key declaration
	 */
	const DEFAULT_BEGIN  = '{';

	/**
	 * the default token that ends a key declaration
	 */
	const DEFAULT_END    = '}';

	/**
	 * the default token that escapes tokens
	 */
	const DEFAULT_ESCAPE = '\\';

	/**
	 * the value of the flag indicating the regular expression state
	 */
	const STATE_REGEX = 1;

	/**
	 * the value of the flag indicating the regular expression escaping state
	 */
	const STATE_REGEX_ESCAPING = 2;

	/**
	 * the value of the flag indicating the key state
	 */
	const STATE_KEY = 3;

	/**
	 * the value of the flag indicating the key escaping state
	 */
	const STATE_KEY_ESCAPING = 4;

	/**
	 * @var string the token being used to begin key declarations
	 */
	private $begin;

	/**
	 * @var string the token being used to end key declarations
	 */
	private $end;

	/**
	 * @var string the token being used to escape tokens
	 */
	private $escape;

	/**
	 * @var string|null the state of the key being extracted from the current
	 * pattern (if any)
	 */
	private $key;

	/**
	 * @var bool whether or not the next token is being escaped in the current
	 * pattern
	 */
	private $escaping;

	/**
	 * @var string the state of the regular expression being extracted from the
	 * current pattern
	 */
	private $regex;

	/**
	 * @var array the keys extracted from the current pattern
	 */
	private $keys;

	/**
	 * Constructs an interpreter of patterns.
	 *
	 * @param string|null $begin the token to use in order to begin key
	 * declaration (or NULL to use the default)
	 * @param string|null $end the token to use in order to end key
	 * declarations (or NULL to use the default)
	 * @param string|null $escape the token to use in order to escape tokens
	 * (or NULL to use the default)
	 */
	public function __construct($begin = null, $end = null, $escape = null)
	{
		if (is_null($begin)) {
			$begin = self::DEFAULT_BEGIN;
		}

		if (is_null($end)) {
			$end = self::DEFAULT_END;
		}

		if (is_null($escape)) {
			$escape = self::DEFAULT_ESCAPE;
		}

		$this->throwExceptionIfInvalidLength(
			InterpreterException::BEGIN_LENGTH,
			$begin
		);

		$this->throwExceptionIfInvalidLength(
			InterpreterException::END_LENGTH,
			$end
		);

		$this->throwExceptionIfInvalidLength(
			InterpreterException::ESCAPE_LENGTH,
			$escape
		);

		if ($begin == $escape) {
			throw new InterpreterException(
				InterpreterException::BEGIN_INDISTINCT
			);
		}

		if ($end == $escape) {
			throw new InterpreterException(
				InterpreterException::END_INDISTINCT
			);
		}

		$this->begin = $begin;
		$this->end = $end;
		$this->escape = $escape;
	}

	public function interpret($pattern)
	{
		$this->key = null;
		$this->escaping = false;
		$this->regex = '';
		$this->keys = array();

		$length = strlen($pattern);

		for ($i = 0; $i < $length; $i++) {
			$this->onToken($pattern[$i]);
		}

		if (!is_null($this->key)) {
			$this->onEnd();
		}

		return new Interpretation("|^{$this->regex}$|", $this->keys);
	}

	/**
	 * Changes the state of the interpreter based on the given token.
	 *
	 * @param string $token the token to base the change of state upon
	 */
	private function onToken($token) {

		switch ($token) {
			case $this->begin:
				$this->onBegin();
				break;
			case $this->end:
				$this->onEnd();
				break;
			case $this->escape:
				$this->onEscape();
				break;
			default:
				$this->onMiscellaneous($token);
				break;
		}
	}

	/**
	 * Changes the state of the interpreter with the assumption that a key
	 * declaration has begun.
	 */
	private function onBegin()
	{
		switch ($this->state()) {
			case self::STATE_REGEX:
				$this->key = '';
				break;
			case self::STATE_REGEX_ESCAPING:
				$this->regex .= preg_quote($this->begin);
				$this->escaping = false;
				break;
			case self::STATE_KEY:
				$this->key .= $this->begin;
				break;
			case self::STATE_KEY_ESCAPING:
				$this->key .= "{$this->escape}{$this->begin}";
				$this->escaping = false;
				break;
		}
	}

	/**
	 * Changes the state of the interpreter with the assumption that a key
	 * declaration has ended.
	 */
	private function onEnd()
	{
		switch ($this->state()) {
			case self::STATE_REGEX:
				$this->regex .= preg_quote($this->end);
				break;
			case self::STATE_REGEX_ESCAPING:
				$this->regex .= preg_quote("{$this->escape}{$this->end}");
				$this->escaping = false;
				break;
			case self::STATE_KEY:
				$this->keys[] = $this->key;
				$this->regex .= '(.*?)';
				$this->key = null;
				break;
			case self::STATE_KEY_ESCAPING:
				$this->key .= $this->end;
				$this->escaping = false;
				break;
		}
	}

	/**
	 * Changes the state of the interpreter with the assumption that a token
	 * is being escaped.
	 */
	private function onEscape()
	{
		switch ($this->state()) {
			case self::STATE_REGEX:
			case self::STATE_KEY:
				$this->escaping = true;
				break;
			case self::STATE_REGEX_ESCAPING:
				$this->regex .= preg_quote(str_repeat($this->escape, 2));
				$this->escaping = false;
				break;
			case self::STATE_KEY_ESCAPING:
				$this->key .= str_repeat($this->escape, 2);
				$this->escaping = false;
				break;
		}
	}

	/**
	 * Changes the state of the interpreter based on the given miscellaneous
	 * token.
	 *
	 * @param string $token the miscellaneous token to base the change of state
	 * upon
	 */
	private function onMiscellaneous($token)
	{
		switch ($this->state()) {
			case self::STATE_REGEX:
				$this->regex .= preg_quote($token);
				break;
			case self::STATE_REGEX_ESCAPING:
				$this->regex .= preg_quote("{$this->escape}{$token}");
				$this->escaping = false;
				break;
			case self::STATE_KEY:
				$this->key .= $token;
				break;
			case self::STATE_KEY_ESCAPING:
				$this->key .= "{$this->escape}{$token}";
				$this->escaping = false;
				break;
		}
	}

	/**
	 * Determine the current state of the interpreter.
	 *
	 * @return int the current state of the interpreter
	 */
	private function state()
	{
		$state = 0;

		if (is_null($this->key)) {
			if ($this->escaping) {
				$state = self::STATE_REGEX_ESCAPING;
			} else {
				$state = self::STATE_REGEX;
			}
		} elseif ($this->escaping) {
			$state = self::STATE_KEY_ESCAPING;
		} else {
			$state = self::STATE_KEY;
		}

		return $state;
	}

	/**
	 * Throws an exception if the given token is invalid.
	 *
	 * @param int $code the code to associate with the thrown exception
	 * @param string $token the token to validate
	 * @throws InterpreterException the given token has an invalid length
	 */
	private function throwExceptionIfInvalidLength($code, $token)
	{
		if (1 != strlen($token)) {
			throw new InterpreterException($code);
		}
	}
}
